module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        mainblack: '#212121',
        mainwhite: '#fafafa',
        mainred: '#ff5252',
        darkblue: '#536dfe',
        lightblue: '#00a4bf',
        mainyellow: '#f5c269',
        lightgray: '#707070',
        darkgray: '#32485d',
        whitemilk: '#f4f8fb',
      },
    },
  },
  variants: {},
  plugins: [],
};
